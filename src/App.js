import React, { useState } from "react";
import "./main.css";
import Create from "./components/Create";

function App() {
  const styleApp = {
    backgroundColor: "#2B3336",
  };

  const [bgColor, setBgColor] = useState(false);

  const [count, setCount] = useState(1);

  const addTask = () => {
    setBgColor(!bgColor);
  };

  return (
    <div className="app " style={bgColor ? styleApp : null}>
      <div className="header">
        <p>toDoApp</p>
      </div>
      <div className="mode">
        <button onClick={() => addTask()} className="bgColor">
          {bgColor ? "Light Mode" : "Dark Mode"}
        </button>
      </div>

      <Create state={bgColor} />
    </div>
  );
}

export default App;
