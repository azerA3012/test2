import React from "react";

const Task = (props) => {
  let background = {
    backgroundColor: "white",
  };

  let name = "No Task";

  let styleText = {
    color: "black",
  };
  return (
    <div className="task my-2" style={props.bgColor ? background : null}>
      <p style={props.bgColor ? styleText : null}>
        {props.arr.length >= 1 ? props.taskName : name}
      </p>
      {props.arr.length >= 1 ? <button>Completed</button> : null}
    </div>
  );
};

export default Task;
