import React, { useState } from "react";
import Task from "./Task";

const Create = (props) => {
  const [value, setValue] = useState([]);

  const [arr, setArr] = useState([]);

  const styleText = {
    color: "white",
  };

  const changeTaskName = (event) => {
    setValue(event.target.value);
  };

  let count = 0;

  const [taskName, setTaskName] = useState([]);
  const addTask = () => {
    count++;
    let array = [...arr];
    array.push(count);
    setArr(array);
    let arrTask = [...taskName];

    arrTask.push(value);

    setTaskName(arrTask);
  };

  return (
    <div className="components container">
      <div className="create">
        <p style={props.state ? styleText : null}>Add your Task here</p>
        <span></span>
        <input
          onChange={(event) => changeTaskName(event)}
          type="text"
          className="form-control my-3"
          placeholder="Enter Task here.."
        />
        <button onClick={() => addTask()} className="btn btn-info">
          Add
        </button>
      </div>

      <div className="page">
        {arr.length >= 1 ? (
          arr.map((item, index) => {
            return (
              <Task
                taskName={taskName[index]}
                arr={arr}
                key={index}
                bgColor={props.state}
              />
            );
          })
        ) : (
          <Task arr={arr} bgColor={props.state} />
        )}
      </div>
    </div>
  );
};

export default Create;
